﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GameScore;
using GameScore.Controllers;
using GameScore.Models;

namespace GameScore.Tests.Controllers
{
    [TestClass]
    public class ValuesControllerTest
    {
        [TestMethod]
        public void TestScore()
        {
            object jsonData = "[{\"first\":3, \"second\": 4}, {\"first\": 4, \"second\": 5}]";
            int expected = 0;

            var controller = new ValuesController();            

            int result = controller.GetScore(jsonData);

            Assert.AreEqual(expected, result);
        }
    }
}
