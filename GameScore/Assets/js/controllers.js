﻿var app = angular.module('myApp', []);
app.controller('scoreCtrl', function ($scope, $http) {

    $scope.frames = [];
    $scope.scoreFinal;
    $scope.error;

    $scope.onAdd = function () {

        if (!checkValid()) {
            //alert("Please enter a score.");
        }
        else {
            $scope.frames.push({
                "First": $scope.scoreFirst == null ? 0 : $scope.scoreFirst,
                "Second": $scope.scoreSecond == null ? 0 : $scope.scoreSecond
            });

            $scope.scoreFirst = null;
            $scope.scoreSecond = null;


            $http.post("api/values/GetScore", angular.toJson($scope.frames)).success(function (data) {
                if (data > 0)                    
                    $scope.scoreFinal = data;
            }).error(function (data) {
                console.log(data);
                $scope.error = "Something wrong when adding new score " + data.ExceptionMessage;
            });
        }
    };

    function checkValid() {
        if ($scope.scoreFirst == null && $scope.scoreSecond == null)
            return false;
        else return true;
    }

    
});