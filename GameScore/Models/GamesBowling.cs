﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameScore.Models
{
    [Serializable]
    public class GamesBowling
    {
        public GamesBowling()
        {

        }

        public IList<Frames> Frame { get; set; }

        public int Score
        {
            get
            {
                int FinalScore = 0;                
                int Count = 0;

                foreach (Frames obj in Frame)
                {
                    int FrameTotal = 0;
                    if (obj.First == 10 || obj.Second == 10)
                    {
                        FrameTotal += 10;

                        for (int i = Count + 1; i <= Frame.Count - 1; i++)
                        {
                            FrameTotal += Frame[i].First;
                            FrameTotal += Frame[i].Second;
                        }
                    }
                    else
                    {
                        FrameTotal += obj.First;
                        FrameTotal += obj.Second;
                    }

                    FinalScore += FrameTotal;

                    Count++;
                }
                return FinalScore;
            }
        }
    }

    [Serializable]
    public class Frames
    {
        public Frames()
        {
            First = 0;
            Second = 0;
        }
        public int First { get; set; }
        public int Second { get; set; }
    }
}