﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GameScore.Models;
using Newtonsoft.Json;

namespace GameScore.Controllers
{
    public class ValuesController : ApiController
    {
        [HttpPost]
        [Route("api/Values/GetScore")]
        public int GetScore(object value)
        {
            GamesBowling gameObj = null;
            try
            {
                string json = JsonConvert.SerializeObject(value);

                gameObj = new GamesBowling
                {
                    Frame = JsonConvert.DeserializeObject<IList<Frames>>(json)
                };
            }
            catch (Exception e)
            {
                //error log
                throw e;
            }

            if (gameObj == null)
                return 0;
            else
                return gameObj.Score;
        }
    }
}
